import React, { Component } from "react";
import ListInput from "../List/ListInput";
import ListBtn from "../List/ListBtn";
import "../../styles/MainBody.css";
import { createTrelloList, getTrelloList } from "../../data/getTrelloData";
import ListDiv  from "../List/ListDiv";

class MainBody extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.id,
            showInput: false,
            inputName:"",
            listData: [],
        }
    }
    handleClose = () => {
        this.setState({
            showInput: false
        })
    }

    handleClick = () => {
        this.setState({
            showInput: true
        })
    }
    getName = (e) => {
        this.setState({
            inputName: e.target.value.trim() 
        })
    }

    getInputValue = (e) => {
        const listName = this.state.inputName;
        if(listName) {
            createTrelloList(listName)
            .then(response => response.json())
            .then(data => {
                this.setState( {
                    listData: [...this.state.listData, data],
                    inputName:''
                })
            })  
            .catch(console.log)
        }else {
            return
        }
    }
    componentDidMount = () => {
        getTrelloList()
        .then(response => response.json())
        .then(data => {
            this.setState({
                listData: data
            })
        })
        .catch(console.log)
    }


    render() {
        return (
            <div className="main-board">
               {this.state.listData.length > 0 ? <ListDiv  listData={this.state.listData}/> : <p className="loader">Loading existing cards...</p> }             
               {this.state.showInput ? <ListInput clickEvent={this.handleClose} getName={this.getName} val={this.state.inputName} getInputValue={this.getInputValue} /> : <ListBtn clickEvent={this.handleClick}/>}
            </div>
        )
    }


}

export default MainBody;