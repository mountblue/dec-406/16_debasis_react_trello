import React, { Component } from "react";
import { CardHolder } from "./Board/CardHolder";
import Header from "./Header/Header";
import { getBoardData } from "../data/getTrelloData";


class  Container extends Component {
    constructor(props) {
        super(props)
        this.state = {
            boardData: ''
        }
    }
    
    componentDidMount() {
        getBoardData().then(resp => resp.json())
        .then(data => {
            this.setState({
                boardData: data
            })
        })
    }

    render() {
        const { name, id } = this.state.boardData;

        return (
            <React.Fragment>
                <Header title={name}/>
                <CardHolder id={id} />
            </React.Fragment>
        )
    }
}



export default Container;