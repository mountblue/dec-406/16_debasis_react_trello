import React from "react";
import "../../styles/ButtonStyle.css"

export default ({clickEvent}) => (
    <div className="btn-style" onClick={() => clickEvent()} >
        <p><span>+</span> Add another list</p>
    </div>
)