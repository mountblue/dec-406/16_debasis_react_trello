import React from "react";
import "../../styles/ListInput.css";

export default ({getName, getInputValue, clickEvent, val}) => (
    <div className="create-list">
            <form className="createList" onSubmit={(e) => e.preventDefault()}>
                <input onChange={getName} value={val} type="text" /> 
            </form>
            <div className="extra-content">
            <input type="submit" value="Add list" className="create-btn" onClick={() => getInputValue()}/>
            <p className="close" onClick={() => clickEvent()}>X</p>
            </div>
    </div>
)

