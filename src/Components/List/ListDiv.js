import React, { Component } from "react";
import "../../styles/ListDiv.css";
import CardDiv from "../Card/CardDiv";
import Card from "../Card/Card";
import { createCard, getAllCards } from "../../data/getTrelloData";

export default class ListDiv extends Component  {
    constructor(props) {
        super(props)
        this.state = {
            listIds: this.props.listData.map(el => el.id),
            cardData: []
        }
    }

    getCardName = (name, id) => {
        if(name && id) {
            createCard(name, id)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    cardData: [...this.state.cardData,  data]
                })
            })
            .catch(console.log)
        } else {
            return
        }
    }

    componentDidMount() {
        this.state.listIds.forEach(element => {
            getAllCards(element)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    cardData: data
                })
            })
            .catch(console.log)
        });
    }

    render() {
       return (
            this.props.listData.map((data,i) => {
            return (
                <div className="list-div" key={String(Symbol(i))}  >
                    <p className="head-name">{data.name}</p>
                    {this.state.cardData.length > 0 && <Card data={this.state.cardData.filter(el => el.idList === data.id)}/>}
                    <CardDiv id={data.id} getName={this.getCardName}/>
                </div>
            )
        })
      )
    }
    
}