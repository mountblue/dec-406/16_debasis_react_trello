import React from 'react';
import "../../styles/Header.css";

const DenseAppBar = ({title}) => (
    <div className="navigation">
        <h4>{title}</h4>
    </div>
)

export default DenseAppBar;