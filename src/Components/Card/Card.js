import React from "react";
import "../../styles/Card.css";
import { FaEdit } from 'react-icons/fa';

export default ({data}) => {
    return  data.map((card,i) => {
        return (
            <div key={String(Symbol(i))} className="single-card" id={card.id} onClick={() => alert("Don't click me")} > 
            <p className="name">{card.name}</p>
            <p className="edit"><FaEdit /></p>
            </div>
        )
    })
}