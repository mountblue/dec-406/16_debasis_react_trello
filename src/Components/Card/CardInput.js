import React from "react";
import "../../styles/CardInput.css";

export const InputCard = ({close, change, click, val}) => {
    return (
        <div className="card-holder">
            <div className="card">
                <input type="text" onSubmit={(e) => e.preventDefault()}  value={val} onChange={change} placeholder="Add a new card here ..." />
            </div>
            <div className="card-content">
                <input type="submit" onClick={click} value="Add a card" className="btn" />
                <p className="close-card"><span onClick={() => close()}>X</span></p>
            </div>
        </div>
    )
}