export const getBoardData = () => {
    return fetch(`https://api.trello.com/1/boards/${process.env.REACT_APP_BOARD}?key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_AUTH_TOKEN}`,{
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    })
   
}

export const getTrelloList = () => {
    return fetch(`https://api.trello.com/1/boards/${process.env.REACT_APP_BOARD}/lists?cards=none&card_fields=all&filter=open&fields=all&key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_AUTH_TOKEN}`, {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    }
    )
}

export const createTrelloList = (name) => {
    return fetch(`https://api.trello.com/1/boards/${process.env.REACT_APP_BOARD}/lists?name=${name}&pos=bottom&key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_AUTH_TOKEN}`, {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        method: 'POST'
    }
    )
}

export const createCard = (name, id) => {
    return fetch(`https://api.trello.com/1/cards?name=${name}&idList=${id}&keepFromSource=all&key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_AUTH_TOKEN}`,
    {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        method: 'POST'
    }
    
    )
}

export const getAllCards = (id) => {
    return fetch(`https://api.trello.com/1/lists/${id}/cards?key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_AUTH_TOKEN}`, {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    })
}